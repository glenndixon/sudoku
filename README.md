Glenn's Jigsaw Sudoku
------------------------

I thought regular Sudoku would be too easy so I implemented Jigsaw Sudoku. The same row and column constraints exists but the "regions" are irregularly shaped. Standard Sudoku is a subset of Jigsaw Sudoku. In other words you could define the regions of a Jigsaw Sudoku puzzle where the 9 regions are all 3x3 grids.

## Demo

[Demo](http://glenndixon.bitbucket.org)

[Screenshot](http://glenndixon.bitbucket.org/screenshot.png)

Bitbucket pages seem to 502 an awful lot so you might have to refresh if it doesn't show up right the first time.

### Demo Locally

I committed the compiled js and css so it would be easy to demo locally. Just serve the `www` directory with your simple http server of choice. For example:

```
cd www
python -m SimpleHTTPServer
```

## Features

### Arrow keys

I thought it was crucial for this type of puzzle that you can easily navigate the puzzle using the arrow keys on the keyboard.

### Mobile friendly-ish

Really, the only thing that makes this mobile friendly feature is the `viewport` tag which sets the zoom to an optimal value for the app.

## Trade-offs

If I had more time I would implement the following features:

*   More puzzles. Currently there's only one puzzle because its sufficient to demo the functionality of the app.
*   Persistence. If you refresh the page or close the tab and come back later, the user's progress is completely lost. Wouldn't be too hard to persist the board state to localStorage.
*   Solver/pre-validator. Perhaps the user would like to know part way through their solution if the progress they made is correct. Or maybe they get bored and just want to see the solution. Right now, the only way to check if your solution is correct is to actually solve the puzzle yourself and see if it passes.

## Implementation

The app is completely rendered on the client. So the only thing `index.html` does is include the dependencies in the `<head>` tag and create an instance of `SudokuApp`.

### The Coffee

The `SudokuApp` class is the controller for the app. It creates an instace of `Puzzle` from the first and only item in `PUZZLE_DATA`, renders the html for the app, handles events from the user, and updates the UI accordingly.

### The Scss

I am a huge fan of [this article](http://engineering.appfolio.com/2012/11/16/css-architecture/) on CSS architecture. It outlines organizing your CSS based on components, sub-components, and modifier classes. For this app, it seemed to make sense that the page itself was one component which comprised the header, the board, and the success/error messages and their layouts; and the cell (`.sudoku-cell`) which seemed to have enough going on which justified having its own component definition.

## Technologies used

### Grunt

This is literally the first time I've ever used [Grunt](http://gruntjs.com/). I figured this was a good excuse to try it out. So pardon me if I didn't something silly.

In order to develop this, I needed a CoffeeScript compiler, a Sass compiler, and way to create a simple http server to serve the app. With Grunt, I can put all those dependences in a `package.json` file. Someone new to the project just has to run `npm install` at the project root to install all of these dependencies. Then in my `Gruntfile.js` I can just define some handy commands. Such as:

Builds the coffee and sass into the `www` directory:
```
grunt
```

Creates an http server for my `www` directory:
```
grunt serve
```

Builds the coffee and sass files, creates the http server, and watches the `.coffee` and `.scss` files and runs the build task when they change:
```
grunt develop
```

### teacup
[Teacup](https://github.com/goodeggs/teacup) is a little known but awesome templating library for CoffeeScript. It allows you to generate HTML with just pure CoffeeScript which is helpful for a variety of reasons.

### Font Awesome
[Font Awesome](http://fortawesome.github.io/Font-Awesome/) is a framework which allows you to easily include font-based icons in any project. You just have to add one link tag and you can start including tons of icons which you can make any size or color. Super helpful for retina support since the font glyphs are vector based.
