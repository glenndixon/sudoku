(function() {
  var SudokuApp,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  SudokuApp = (function() {
    function SudokuApp() {
      this._handle_keydown = __bind(this._handle_keydown, this);
      this._handle_text_input = __bind(this._handle_text_input, this);
      this.$el = $("body");
      this._setup_handlers();
      this.puzzle = new SudokuPuzzle(PUZZLE_DATA[0]);
      this.$el.html(app_template(this.puzzle));
    }

    SudokuApp.prototype._setup_handlers = function() {
      this.$el.on("keydown", ".sudoku-cell__input", this._handle_keydown);
      return this.$el.on("input propertychange", ".sudoku-cell__input", this._handle_text_input);
    };

    SudokuApp.prototype._field_summary = function() {
      var any_empty_fields, error_fields, values;
      any_empty_fields = false;
      values = [];
      this.$el.find(".sudoku-cell").each(function(index) {
        var $input, val;
        $input = $(this).find(".sudoku-cell__input");
        if ($input.length) {
          val = $input.val();
          if (val.length === 0) {
            any_empty_fields = true;
          }
          return values.push($input.val());
        } else {
          return values.push(null);
        }
      });
      error_fields = this.puzzle.check_for_errors(values);
      return [any_empty_fields, error_fields];
    };

    SudokuApp.prototype._validate_board_state = function() {
      var $cells, $error_area, $success_area, any_empty_fields, error_fields, i, _i, _len, _ref;
      _ref = this._field_summary(), any_empty_fields = _ref[0], error_fields = _ref[1];
      $error_area = this.$el.find(".sudoku-page__error-area").hide();
      $success_area = this.$el.find(".sudoku-page__success-area").hide();
      $cells = this.$el.find(".sudoku-cell");
      $cells.find(".sudoku-cell__input").removeClass("m-has-error");
      if (error_fields.length) {
        for (_i = 0, _len = error_fields.length; _i < _len; _i++) {
          i = error_fields[_i];
          $($cells[i]).find(".sudoku-cell__input").addClass("m-has-error");
        }
        return $error_area.show();
      } else if (!any_empty_fields) {
        return $success_area.show();
      }
    };

    SudokuApp.prototype._handle_text_input = function(e) {
      this._validate_board_state();
    };

    SudokuApp.prototype._handle_keydown = function(e) {
      var $cell, $input, $inputs, HORIZONTAL, VERTICAL, action, actions, col, direction, index, row, type;
      VERTICAL = 1;
      HORIZONTAL = 2;
      actions = {
        37: [VERTICAL, -1],
        38: [HORIZONTAL, -1],
        39: [VERTICAL, 1],
        40: [HORIZONTAL, 1]
      };
      action = actions[e.which];
      if (action) {
        type = action[0], direction = action[1];
        $input = $(e.currentTarget);
        $cell = $input.closest(".sudoku-cell");
        if (type === HORIZONTAL) {
          col = $cell.data("col");
          $inputs = this.$el.find(".sudoku-cell[data-col=" + col + "] .sudoku-cell__input");
        } else {
          row = $cell.data("row");
          $inputs = this.$el.find(".sudoku-cell[data-row=" + row + "] .sudoku-cell__input");
        }
        index = $inputs.index($input) + direction;
        index %= $inputs.length;
        $inputs.eq(index).focus();
      }
    };

    return SudokuApp;

  })();

  window.SudokuApp = SudokuApp;

}).call(this);

(function() {
  var SudokuPuzzle;

  SudokuPuzzle = (function() {
    function SudokuPuzzle(_arg) {
      this.regions = _arg.regions, this.initial_state = _arg.initial_state;
    }

    SudokuPuzzle.prototype.initial_state_for_cell = function(col, row) {
      return this.initial_state[col + row * 9];
    };

    SudokuPuzzle.prototype.region_id_for_cell = function(col, row) {
      return this.regions[col + row * 9];
    };

    SudokuPuzzle.prototype.check_for_errors = function(values) {
      var col, error_fields, i, indices_to_validate, region_id, row, val, values_by_col, values_by_region, values_by_row, _i, _j, _len;
      error_fields = [];
      values_by_region = this._empty_matrix();
      values_by_row = this._empty_matrix();
      values_by_col = this._empty_matrix();
      indices_to_validate = [];
      for (i = _i = 0; _i <= 80; i = ++_i) {
        region_id = this.regions[i];
        if (values[i]) {
          if (!values[i].match(/[1-9]/)) {
            error_fields.push(i);
            continue;
          }
        }
        val = null;
        if (this.initial_state[i]) {
          val = this.initial_state[i];
        } else if (values[i]) {
          val = +values[i];
          indices_to_validate.push(i);
        }
        if (val) {
          values_by_region[region_id].push(val);
          values_by_col[i % 9].push(val);
          values_by_row[Math.floor(i / 9)].push(val);
        }
      }
      for (_j = 0, _len = indices_to_validate.length; _j < _len; _j++) {
        i = indices_to_validate[_j];
        region_id = this.regions[i];
        col = i % 9;
        row = Math.floor(i / 9);
        val = +values[i];
        if (this._is_value_duplicated(val, values_by_region[region_id]) || this._is_value_duplicated(val, values_by_col[col]) || this._is_value_duplicated(val, values_by_row[row])) {
          error_fields.push(i);
        }
      }
      return error_fields;
    };

    SudokuPuzzle.prototype._is_value_duplicated = function(value, arr) {
      var a, count, _i, _len;
      count = 0;
      for (_i = 0, _len = arr.length; _i < _len; _i++) {
        a = arr[_i];
        if (value === a) {
          count++;
        }
      }
      return count > 1;
    };

    SudokuPuzzle.prototype._empty_matrix = function() {
      var i, matrix, _i;
      matrix = [];
      for (i = _i = 0; _i <= 8; i = ++_i) {
        matrix.push([]);
      }
      return matrix;
    };

    return SudokuPuzzle;

  })();

  window.SudokuPuzzle = SudokuPuzzle;

}).call(this);

(function() {
  var PUZZLE_DATA;

  PUZZLE_DATA = [
    {
      regions: [0, 0, 0, 1, 2, 2, 2, 2, 2, 0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 3, 3, 3, 3, 1, 1, 1, 2, 0, 0, 3, 4, 4, 4, 4, 1, 1, 3, 3, 3, 3, 4, 5, 5, 5, 5, 6, 6, 4, 4, 4, 4, 5, 7, 7, 8, 6, 6, 6, 5, 5, 5, 5, 7, 8, 8, 8, 6, 6, 6, 7, 7, 7, 8, 8, 8, 8, 8, 6, 7, 7, 7],
      initial_state: [3, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 2, 0, 6, 0, 1, 0, 0, 0, 1, 0, 9, 0, 8, 0, 2, 0, 0, 0, 5, 0, 0, 0, 6, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0, 0, 0, 9, 0, 0, 0, 8, 0, 0, 0, 8, 0, 3, 0, 4, 0, 6, 0, 0, 0, 4, 0, 1, 0, 9, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 7]
    }
  ];

  window.PUZZLE_DATA = PUZZLE_DATA;

}).call(this);

(function() {
  var Teacup, doctypes, elements, merge_elements, tagName, _fn, _fn1, _fn2, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2,
    __slice = [].slice,
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  doctypes = {
    'default': '<!DOCTYPE html>',
    '5': '<!DOCTYPE html>',
    'xml': '<?xml version="1.0" encoding="utf-8" ?>',
    'transitional': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
    'strict': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
    'frameset': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
    '1.1': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
    'basic': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">',
    'mobile': '<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">',
    'ce': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "ce-html-1.0-transitional.dtd">'
  };

  elements = {
    regular: 'a abbr address article aside audio b bdi bdo blockquote body button canvas caption cite code colgroup datalist dd del details dfn div dl dt em fieldset figcaption figure footer form h1 h2 h3 h4 h5 h6 head header hgroup html i iframe ins kbd label legend li map mark menu meter nav noscript object ol optgroup option output p pre progress q rp rt ruby s samp section select small span strong sub summary sup table tbody td textarea tfoot th thead time title tr u ul video',
    raw: 'script style',
    "void": 'area base br col command embed hr img input keygen link meta param source track wbr',
    obsolete: 'applet acronym bgsound dir frameset noframes isindex listing nextid noembed plaintext rb strike xmp big blink center font marquee multicol nobr spacer tt',
    obsolete_void: 'basefont frame'
  };

  merge_elements = function() {
    var a, args, element, result, _i, _j, _len, _len1, _ref;
    args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
    result = [];
    for (_i = 0, _len = args.length; _i < _len; _i++) {
      a = args[_i];
      _ref = elements[a].split(' ');
      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        element = _ref[_j];
        if (__indexOf.call(result, element) < 0) {
          result.push(element);
        }
      }
    }
    return result;
  };

  Teacup = (function() {
    function Teacup() {
      this.htmlOut = null;
    }

    Teacup.prototype.resetBuffer = function(html) {
      var previous;
      if (html == null) {
        html = null;
      }
      previous = this.htmlOut;
      this.htmlOut = html;
      return previous;
    };

    Teacup.prototype.render = function() {
      var args, previous, result, template;
      template = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      previous = this.resetBuffer('');
      try {
        template.apply(null, args);
      } finally {
        result = this.resetBuffer(previous);
      }
      return result;
    };

    Teacup.prototype.cede = function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return this.render.apply(this, args);
    };

    Teacup.prototype.renderable = function(template) {
      var teacup;
      teacup = this;
      return function() {
        var args, result;
        args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
        if (teacup.htmlOut === null) {
          teacup.htmlOut = '';
          try {
            template.apply(this, args);
          } finally {
            result = teacup.resetBuffer();
          }
          return result;
        } else {
          return template.apply(this, args);
        }
      };
    };

    Teacup.prototype.renderAttr = function(name, value) {
      var k, v;
      if (value == null) {
        return " " + name;
      }
      if (value === false) {
        return '';
      }
      if (name === 'data' && typeof value === 'object') {
        return ((function() {
          var _results;
          _results = [];
          for (k in value) {
            v = value[k];
            _results.push(this.renderAttr("data-" + k, v));
          }
          return _results;
        }).call(this)).join('');
      }
      if (value === true) {
        value = name;
      }
      return " " + name + "=" + (this.quote(this.escape(value.toString())));
    };

    Teacup.prototype.attrOrder = ['id', 'class'];

    Teacup.prototype.renderAttrs = function(obj) {
      var name, result, value, _i, _len, _ref;
      result = '';
      _ref = this.attrOrder;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        if (!(name in obj)) {
          continue;
        }
        result += this.renderAttr(name, obj[name]);
        delete obj[name];
      }
      for (name in obj) {
        value = obj[name];
        result += this.renderAttr(name, value);
      }
      return result;
    };

    Teacup.prototype.renderContents = function(contents) {
      var result;
      if (contents == null) {

      } else if (typeof contents === 'function') {
        result = contents.call(this);
        if (typeof result === 'string') {
          return this.text(result);
        }
      } else {
        return this.text(contents);
      }
    };

    Teacup.prototype.isSelector = function(string) {
      var _ref;
      return string.length > 1 && ((_ref = string.charAt(0)) === '#' || _ref === '.');
    };

    Teacup.prototype.parseSelector = function(selector) {
      var classes, id, klass, token, _i, _len, _ref, _ref1;
      id = null;
      classes = [];
      _ref = selector.split('.');
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        token = _ref[_i];
        if (id) {
          classes.push(token);
        } else {
          _ref1 = token.split('#'), klass = _ref1[0], id = _ref1[1];
          if (klass !== '') {
            classes.push(token);
          }
        }
      }
      return {
        id: id,
        classes: classes
      };
    };

    Teacup.prototype.normalizeArgs = function(args) {
      var arg, attrs, classes, contents, id, index, selector, _i, _len;
      attrs = {};
      selector = null;
      contents = null;
      for (index = _i = 0, _len = args.length; _i < _len; index = ++_i) {
        arg = args[index];
        if (arg != null) {
          switch (typeof arg) {
            case 'string':
              if (index === 0 && this.isSelector(arg)) {
                selector = this.parseSelector(arg);
              } else {
                contents = arg;
              }
              break;
            case 'function':
            case 'number':
            case 'boolean':
              contents = arg;
              break;
            case 'object':
              if (arg.constructor === Object) {
                attrs = arg;
              } else {
                contents = arg;
              }
              break;
            default:
              contents = arg;
          }
        }
      }
      if (selector != null) {
        id = selector.id, classes = selector.classes;
        if (id != null) {
          attrs.id = id;
        }
        if (classes != null ? classes.length : void 0) {
          if (attrs["class"]) {
            classes.push(attrs["class"]);
          }
          attrs["class"] = classes.join(' ');
        }
      }
      return {
        attrs: attrs,
        contents: contents
      };
    };

    Teacup.prototype.tag = function() {
      var args, attrs, contents, tagName, _ref;
      tagName = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      _ref = this.normalizeArgs(args), attrs = _ref.attrs, contents = _ref.contents;
      this.raw("<" + tagName + (this.renderAttrs(attrs)) + ">");
      this.renderContents(contents);
      return this.raw("</" + tagName + ">");
    };

    Teacup.prototype.rawTag = function() {
      var args, attrs, contents, tagName, _ref;
      tagName = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      _ref = this.normalizeArgs(args), attrs = _ref.attrs, contents = _ref.contents;
      this.raw("<" + tagName + (this.renderAttrs(attrs)) + ">");
      this.raw(contents);
      return this.raw("</" + tagName + ">");
    };

    Teacup.prototype.selfClosingTag = function() {
      var args, attrs, contents, tag, _ref;
      tag = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      _ref = this.normalizeArgs(args), attrs = _ref.attrs, contents = _ref.contents;
      if (contents) {
        throw new Error("Teacup: <" + tag + "/> must not have content.  Attempted to nest " + contents);
      }
      return this.raw("<" + tag + (this.renderAttrs(attrs)) + " />");
    };

    Teacup.prototype.coffeescript = function(fn) {
      return this.raw("<script type=\"text/javascript\">(function() {\n  var __slice = [].slice,\n      __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },\n      __hasProp = {}.hasOwnProperty,\n      __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };\n  (" + (fn.toString()) + ")();\n})();</script>");
    };

    Teacup.prototype.comment = function(text) {
      return this.raw("<!--" + (this.escape(text)) + "-->");
    };

    Teacup.prototype.doctype = function(type) {
      if (type == null) {
        type = 5;
      }
      return this.raw(doctypes[type]);
    };

    Teacup.prototype.ie = function(condition, contents) {
      this.raw("<!--[if " + (this.escape(condition)) + "]>");
      this.renderContents(contents);
      return this.raw("<![endif]-->");
    };

    Teacup.prototype.text = function(s) {
      if (this.htmlOut == null) {
        throw new Error("Teacup: can't call a tag function outside a rendering context");
      }
      this.htmlOut += (s != null) && this.escape(s.toString()) || '';
      return null;
    };

    Teacup.prototype.raw = function(s) {
      if (s == null) {
        return;
      }
      this.htmlOut += s;
      return null;
    };

    Teacup.prototype.escape = function(text) {
      return text.toString().replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    };

    Teacup.prototype.quote = function(value) {
      return "\"" + value + "\"";
    };

    Teacup.prototype.tags = function() {
      var bound, boundMethodNames, method, _fn, _i, _len;
      bound = {};
      boundMethodNames = [].concat('cede coffeescript comment doctype escape ie normalizeArgs raw render renderable script tag text'.split(' '), merge_elements('regular', 'obsolete', 'raw', 'void', 'obsolete_void'));
      _fn = (function(_this) {
        return function(method) {
          return bound[method] = function() {
            var args;
            args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
            return _this[method].apply(_this, args);
          };
        };
      })(this);
      for (_i = 0, _len = boundMethodNames.length; _i < _len; _i++) {
        method = boundMethodNames[_i];
        _fn(method);
      }
      return bound;
    };

    return Teacup;

  })();

  _ref = merge_elements('regular', 'obsolete');
  _fn = function(tagName) {
    return Teacup.prototype[tagName] = function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return this.tag.apply(this, [tagName].concat(__slice.call(args)));
    };
  };
  for (_i = 0, _len = _ref.length; _i < _len; _i++) {
    tagName = _ref[_i];
    _fn(tagName);
  }

  _ref1 = merge_elements('raw');
  _fn1 = function(tagName) {
    return Teacup.prototype[tagName] = function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return this.rawTag.apply(this, [tagName].concat(__slice.call(args)));
    };
  };
  for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
    tagName = _ref1[_j];
    _fn1(tagName);
  }

  _ref2 = merge_elements('void', 'obsolete_void');
  _fn2 = function(tagName) {
    return Teacup.prototype[tagName] = function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return this.selfClosingTag.apply(this, [tagName].concat(__slice.call(args)));
    };
  };
  for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
    tagName = _ref2[_k];
    _fn2(tagName);
  }

  if (typeof module !== "undefined" && module !== null ? module.exports : void 0) {
    module.exports = new Teacup().tags();
    module.exports.Teacup = Teacup;
  } else if (typeof define === 'function' && define.amd) {
    define('teacup', [], function() {
      return new Teacup().tags();
    });
  } else {
    window.teacup = new Teacup().tags();
    window.teacup.Teacup = Teacup;
  }

}).call(this);

(function() {
  var app_template;

  app_template = function(puzzle) {
    var div, h1, i, input, render, text, _board, _cell;
    div = teacup.div, h1 = teacup.h1, render = teacup.render, text = teacup.text, input = teacup.input, i = teacup.i;
    _cell = function(row, col) {
      var attrs, region_id;
      region_id = puzzle.region_id_for_cell(col, row);
      attrs = {
        'data-row': row,
        'data-col': col
      };
      return div(".sudoku-cell.m-region-" + region_id, attrs, function() {
        var initial_state;
        initial_state = puzzle.initial_state_for_cell(col, row);
        if (initial_state) {
          div('.sudoku-cell__initial-value', function() {
            return text(initial_state);
          });
        } else {
          input('.sudoku-cell__input', {
            type: "text",
            pattern: "[0-9]*",
            maxlength: 1
          });
        }
        if (col < 8 && region_id !== puzzle.region_id_for_cell(col + 1, row)) {
          div('.sudoku-cell__right-divider');
        }
        if (row < 8 && region_id !== puzzle.region_id_for_cell(col, row + 1)) {
          return div('.sudoku-cell__bottom-divider');
        }
      });
    };
    _board = function() {
      var col, row, _i, _j, _results;
      _results = [];
      for (row = _i = 0; _i <= 8; row = ++_i) {
        for (col = _j = 0; _j <= 8; col = ++_j) {
          _cell(row, col);
        }
        _results.push(div('.clear'));
      }
      return _results;
    };
    return render(function() {
      return div('.sudoku-page', function() {
        h1(".sudoku-page__h1", function() {
          return text("Jigsaw Sudoku");
        });
        div('.sudoku-page__board-wrapper', function() {
          return div(".sudoku-page__board", function() {
            return _board();
          });
        });
        div('.sudoku-page__error-area', function() {
          i(".fa.fa-exclamation-triangle.fa-spin.sudoku-page__icon");
          text("You made an error. I'm so sorry.");
          return i(".fa.fa-exclamation-triangle.fa-spin.sudoku-page__icon");
        });
        return div('.sudoku-page__success-area', function() {
          i(".fa.fa-star.fa-spin.sudoku-page__icon");
          text("You solved it. Congrats.");
          return i(".fa.fa-star.fa-spin.sudoku-page__icon");
        });
      });
    });
  };

  window.app_template = app_template;

}).call(this);
