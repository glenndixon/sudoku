module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                files: {
                    'www/main.css' : 'sass/main.scss'
                }
            }
        },
        coffee: {
            compile: {
                files: {
                    'www/main.js': 'coffee/*.coffee',
                }
            }
        },
        watch: {
            css: {
                files: 'sass/*.scss',
                tasks: ['sass']
            },
            coffee: {
                files: 'coffee/*.coffee',
                tasks: ['coffee']
            }
        },
        'http-server': {
            'dev': {
                root: "www/",
                port: 8282,
                host: "127.0.0.1",
                runInBackground: true
            },
            'serve': {
                root: "www/",
                port: 8282,
                host: "127.0.0.1",
                runInBackground: false
            },
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-http-server');
    grunt.registerTask('default',['sass', 'coffee']);
    grunt.registerTask('develop',['sass', 'coffee', 'http-server:dev', 'watch']);
    grunt.registerTask('serve', ['http-server:serve']);
}
