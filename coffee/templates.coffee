app_template = (puzzle) ->
    {div, h1, render, text, input, i} = teacup

    _cell = (row, col) ->
        region_id = puzzle.region_id_for_cell(col, row)
        attrs = {
            'data-row': row,
            'data-col': col
        }
        div ".sudoku-cell.m-region-#{region_id}", attrs, ->
            initial_state = puzzle.initial_state_for_cell(col, row)
            if initial_state
                div '.sudoku-cell__initial-value', ->
                    text initial_state
            else
                input '.sudoku-cell__input', {
                    type: "text"
                    pattern: "[0-9]*" # this makes the number pad come up in mobile safari
                    maxlength: 1
                }

            if col < 8 and region_id != puzzle.region_id_for_cell(col + 1, row)
                div '.sudoku-cell__right-divider'

            if row < 8 and region_id != puzzle.region_id_for_cell(col, row + 1)
                div '.sudoku-cell__bottom-divider'

    _board = ->
        for row in [0..8]
            for col in [0..8]
                _cell(row, col)
            div '.clear'

    return render ->
        div '.sudoku-page', ->
            h1 ".sudoku-page__h1", ->
                text "Jigsaw Sudoku"
            div '.sudoku-page__board-wrapper', ->
                div ".sudoku-page__board", ->
                    _board()

            div '.sudoku-page__error-area', ->
                i ".fa.fa-exclamation-triangle.fa-spin.sudoku-page__icon"
                text "You made an error. I'm so sorry."
                i ".fa.fa-exclamation-triangle.fa-spin.sudoku-page__icon"

            div '.sudoku-page__success-area', ->
                i ".fa.fa-star.fa-spin.sudoku-page__icon"
                text "You solved it. Congrats."
                i ".fa.fa-star.fa-spin.sudoku-page__icon"

window.app_template = app_template
