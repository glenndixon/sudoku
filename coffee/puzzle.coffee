class SudokuPuzzle
    constructor: ({@regions, @initial_state}) ->

    initial_state_for_cell: (col, row) ->
        return @initial_state[col + row * 9]

    region_id_for_cell: (col, row) ->
        return @regions[col + row * 9]

    check_for_errors: (values) ->
        error_fields = []

        values_by_region = @_empty_matrix()
        values_by_row = @_empty_matrix()
        values_by_col = @_empty_matrix()

        indices_to_validate = []
        for i in [0..80]
            region_id = @regions[i]

            if values[i]
                if not values[i].match(/[1-9]/)
                    error_fields.push(i)
                    continue

            val = null
            if @initial_state[i]
                val = @initial_state[i]
            else if values[i]
                val = +values[i]
                indices_to_validate.push(i)
            if val
                values_by_region[region_id].push(val)
                values_by_col[i % 9].push(val)
                values_by_row[Math.floor(i / 9)].push(val)

        # indices_to_validate should contain all of the fields with numerical
        # user input. Now we can loop over all the values and check each one
        # for each constraint (row, column, and region uniqueness)
        for i in indices_to_validate
            region_id = @regions[i]
            col = i % 9
            row = Math.floor(i / 9)

            val = +values[i]

            if @_is_value_duplicated(val, values_by_region[region_id]) ||
                        @_is_value_duplicated(val, values_by_col[col]) ||
                        @_is_value_duplicated(val, values_by_row[row])
                error_fields.push(i)

        return error_fields

    _is_value_duplicated: (value, arr) ->
        count = 0
        for a in arr
            if value == a
                count++
        return count > 1

    _empty_matrix: ->
        matrix = []
        matrix.push([]) for i in [0..8]
        return matrix

window.SudokuPuzzle = SudokuPuzzle
