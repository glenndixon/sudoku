class SudokuApp
    constructor: ->
        @$el = $("body")
        @_setup_handlers()

        @puzzle = new SudokuPuzzle(PUZZLE_DATA[0])
        @$el.html(app_template(@puzzle))

    _setup_handlers: ->
        @$el.on "keydown", ".sudoku-cell__input", @_handle_keydown
        @$el.on "input propertychange", ".sudoku-cell__input", @_handle_text_input

    _field_summary: ->
        any_empty_fields = false

        # push all the user inputs into an array
        # check if there are any empty values
        values = []
        @$el.find(".sudoku-cell").each (index) ->
            $input = $(this).find(".sudoku-cell__input")
            if $input.length
                val = $input.val()
                if val.length == 0
                    any_empty_fields = true
                values.push($input.val())
            else
                values.push(null)

        error_fields = @puzzle.check_for_errors(values)

        return [any_empty_fields, error_fields]

    _validate_board_state: ->
        [any_empty_fields, error_fields] = @_field_summary()

        $error_area = @$el.find(".sudoku-page__error-area").hide()
        $success_area = @$el.find(".sudoku-page__success-area").hide()

        $cells = @$el.find(".sudoku-cell")
        $cells.find(".sudoku-cell__input").removeClass("m-has-error")

        if error_fields.length
            # there are errors so show the error area message and add 'm-has-error'
            # to the fields that have errors
            for i in error_fields
                $($cells[i]).find(".sudoku-cell__input").addClass("m-has-error")
            $error_area.show()
        else if not any_empty_fields
            # no errors and no empty fields means: win
            $success_area.show()

    _handle_text_input: (e) =>
        @_validate_board_state()
        return

    # navigate the board using the arrow keys
    _handle_keydown: (e) =>
        VERTICAL = 1
        HORIZONTAL = 2
        actions = {
            37: [VERTICAL,   -1] #left
            38: [HORIZONTAL, -1] #up
            39: [VERTICAL,    1] #right
            40: [HORIZONTAL,  1] #down
        }
        action = actions[e.which]
        if action
            [type, direction] = action
            $input = $(e.currentTarget)
            $cell = $input.closest(".sudoku-cell")

            if type == HORIZONTAL
                col = $cell.data("col")
                $inputs = @$el.find(".sudoku-cell[data-col=#{col}] .sudoku-cell__input")
            else
                row = $cell.data("row")
                $inputs = @$el.find(".sudoku-cell[data-row=#{row}] .sudoku-cell__input")

            index = $inputs.index($input) + direction
            index %= $inputs.length

            $inputs.eq(index).focus()

        return

window.SudokuApp = SudokuApp
